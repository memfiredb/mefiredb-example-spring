package com.memfiredb.springdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.memfiredb.springdemo.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
