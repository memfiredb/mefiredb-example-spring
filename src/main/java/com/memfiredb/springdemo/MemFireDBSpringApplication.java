package com.memfiredb.springdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MemFireDBSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemFireDBSpringApplication.class, args);
	}

}
